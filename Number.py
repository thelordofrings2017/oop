from random import randint

class Number():

    def __init__(self, val = 0):
        self._value = val

    def __str__(self):
        return self._value.__str__()

    def __repr__(self):
        return str(self)
   
    def __index__(self):
        return self._value

    def __add__(self,other):
        return self._value + other

    def __radd__(self,other):
        return other + self._value

    def __sub__(self, other):
        return self._value - other

    def __rsub__(self, other):
        return other - self._value

    def __mul__(self, other):
        return self._value * other

    def __rmul__(self, other):
        return other * self._value

    def __truediv__(self, other):
        return self._value / other

    def __rtruediv__(self, other):
        return  other / self._value
      
    def __pow__(self,other):
        return self._value ** other

    def __rpow__(self,other):
        return other ** self._value

    def __lt__(self, other):
        return self._value < other._value

    def __le__(self, other):
        return self._value <= other._value

    def __eq__(self, other):
        return self._value == other._value

    def __ne__(self, other):
        return self._value != other._value

    def __qt__(self, other):
        return self._value > other._value

    def __ge__(self, other):
        return self._value >= other._value
    
a=[]


for i in range(1, 10):
    n = Number(randint(1 , 99))
    a.append(n)
print(str(a))

b = sorted(a)
print(b)


n = Number(int(input()))
m = Number(int(input()))

print(n+m)
print(n-m)
print(m-n)
print(n*m)
print(n/m)
print(m/n)
print(n**m)
print(m**n)

print(n < m)
print(n <= m)
print(n == m)
print(n != m)
print(n > m)
print(n >= m)

#Впишите операцию в двоичную, восьмиричную и шестнадцатеричную систему счисления для подсчите
#print(bin(n+m)) 
print(bin(n)) 
print(oct(n)) 
print(hex(n))
