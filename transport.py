from pprint import pprint

class Transport:
    def __init__(self, weight, fuelType, maxSpeed, bodyColor):
        self.weight = weight
        self.fuelType = fuelType
        self.bodyColor = bodyColor
        self.maxSpeed = maxSpeed



class Car(Transport):
    def __init__(self, weight, fuelType, maxSpeed, bodyColor, trunkVolume, interiorColor):
        super().__init__(weight, fuelType, maxSpeed, bodyColor)
        self.trunkVolume = trunkVolume
        self.interiorColor = interiorColor
        

    def traffic():
        lst = []
        x = 0
        y = 0
        while x != 'stop' and y != 'stop':
            x = input("Напиши направление движения Машины Вперед или Назад или stop ")
            if x == "Вперед":
                print('Машина проехала вперед')
                lst.append(x)
            else:
                x == "Назад"
                print('Машина проехала назад ')
                lst.append(x)
            print(lst)
            y = input("Напиши направление движения Налево или Напрво или stop ")
            if y == 'Налево':
                print('Машина повернула налево ')
                lst.append(y)
            else:
                y == 'Направо'
                print('Машина повернула направо ')
                lst.append(y)
            print(lst)
        



class Tram(Transport):
    def __init__(self, weight, fuelType, maxSpeed, bodyColor, seat_place, electric_power):
        super().__init__(weight, fuelType, maxSpeed, bodyColor)
        self.seat_place = seat_place
        self.electric_power = electric_power


    def traffic():
        lst = []
        x = 0
        while x != 'stop':
            x = input("Напиши направление движения Трамвая Вперед или Назад или stop ")
            if x == "Вперед":
                print('Трамвай проехала вперед')
                lst.append(x)
            else:
                x == "Назад"
                print('Трамвай проехала назад ')
                lst.append(x)
            print(lst)



c = Car
print(c.traffic())

t = Tram
print(t.traffic())

obj = Car(2000, "dodge challenger", '360km', 'black', 400, 'white')
pprint(obj.__dict__)

print("\n")
obj2 = Tram(20000, 'City_tram', '80km', 'dirty', 40, 17)
pprint(obj2.__dict__)
